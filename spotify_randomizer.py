import logging
from random import shuffle

import click
import spotipy


CLIENT_ID = "70f5bb2d200e4d23913dd3d0df5a0e1f"
REDIRECT_URI = "http://127.0.0.1:59071"
SCOPES = ["playlist-read-private", "playlist-modify-private", "playlist-modify-public"]
IS_PUBLISHED = False


log = logging.getLogger(__name__)


def get_all(func, *args):
    has_more = True
    offset = 0
    while has_more:
        result = func(*args, offset=offset)
        yield from result["items"]
        has_more = result.get("next") is not None
        offset += result["limit"]


def randomize_playlist(client, playlist):
    playlist_length = playlist["tracks"]["total"]
    tracks = list(
        get_all(
            client.playlist_items,
            playlist["id"],
            "limit,items(track(name,artists(name)))",
        )
    )
    track_names = [
        (t["track"]["artists"][0]["name"] + " - " + t["track"]["name"]) for t in tracks
    ]
    positions = list(range(playlist_length))
    shuffle(positions)
    log.info(f"Setting new positions for the {playlist_length} items...")
    while positions:
        old_position = positions.pop(0)
        if old_position == 0:
            continue  # no change, skip
        try:
            log.info(f"Moving item {track_names[old_position]!r} to the beginning")
            client.playlist_reorder_items(
                playlist["id"], range_start=old_position, insert_before=0
            )
            track_names.insert(0, track_names.pop(old_position))
        except spotipy.SpotifyException as e:
            print(e, e.http_status, e.code)
            raise
        # we're changing the list in-place, adjust positions
        positions = [p + 1 if p < old_position else p for p in positions]


def choose_playlist(client):
    log.info("Getting playlists...")
    playlists = list(get_all(client.current_user_playlists))
    search = click.prompt("Please enter playlist name")
    matched = [
        p
        for p in playlists
        if search.lower() in p["name"].lower()
        or search.lower() in p["description"].lower()
    ]
    if not matched:
        click.echo(f"Could not find any playlist matching {search!r}", err=True)
        raise click.Abort()
    click.echo("Choose playlist:")
    for index, playlist in enumerate(matched):
        click.echo(f"{index + 1}. {playlist['name']}")
    selected = click.prompt("Your choice?", type=int)
    return matched[selected - 1]["id"]


@click.command
@click.option(
    "--creds", type=click.Path(), default="creds.json", help="Path to the credential cache"
)
@click.option("--yes", is_flag=True, default=False, help="Do not ask for confirmation")
@click.option("-v", "--verbose", is_flag=True, default=False, help="Show more info")
@click.option("--debug", is_flag=True, default=False, help="Show a ton of info")
@click.argument("playlists", nargs=-1)
def main(playlists, yes, creds, verbose, debug):
    if debug:
        log_level = logging.DEBUG
    elif verbose:
        log_level = logging.INFO
    else:
        log_level = logging.WARNING
    logging.basicConfig(level=log_level, format="%(message)s")
    cache_handler = spotipy.CacheFileHandler(cache_path=creds)
    auth_manager = spotipy.SpotifyPKCE(
        client_id=CLIENT_ID,
        redirect_uri=REDIRECT_URI,
        scope=",".join(SCOPES),
        cache_handler=cache_handler,
    )
    client = spotipy.Spotify(auth_manager=auth_manager)
    if not playlists:
        playlists = [choose_playlist(client)]
    for playlist in playlists:
        playlist = client.playlist(playlist)
        if not yes and not click.confirm(
            f"The playlist {playlist['name']!r} ({playlist['id']}) has "
            f"{playlist['tracks']['total']} songs. Randomize?",
            default=True,
        ):
            log.info(f"Not randomizing {playlist['name']!r}.")
            continue
        randomize_playlist(client, playlist)
    log.info("Done.")


if __name__ == "__main__":
    main()
